#include"queue.h"
#include <iostream>

#define OFFSET 2 //I want my array to be a circuler array. The "pointers" to the front and rear to the queue would be the first 2 cells.
#define FRONT_CELL 0
#define REAR_CELL 1

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);
void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty
bool isEmpty(queue* q);
bool isFull(queue* q);

/*
This func will start a new queue at the givin size.
input: pointer to the queue | size of the queu.
output: none.
*/
void initQueue(queue* q, unsigned int size)
{
	q->arr = new unsigned int[size + OFFSET];
	q->arr[FRONT_CELL] = -1;
	q->arr[REAR_CELL] = -1;
}

/*
This func will delete the queue and clear the memory.
input: pointer to the queue.
output: none.
*/
void cleanQueue(queue* q)
{
	delete[] q->arr;
	delete[] q;
}

/*
This func will determain if the queue is empty.
input: a pointer to a queue.
output: true if empty else false.
*/
bool isEmpty(queue* q)
{
	if (q->arr[FRONT_CELL] == -1 && q->arr[REAR_CELL] == -1 || q->arr[FRONT_CELL] == q->arr[REAR_CELL])
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
This func will checks if the queue is full.
input: pointer to a queue.
output: true if full else false.
*/
bool isFull(queue* q)
{
	int front = q->arr[FRONT_CELL], rear = q->arr[REAR_CELL], size = (sizeof(q->arr)) / (sizeof((q->arr)[0]));

	if ( ((rear + 1) % size == 0 )|| (rear + 1 == front))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
This func will enqueue a new number to the queue.
input: pointer to a queue | the value to enqueue.
output: none.
*/
void enqueue(queue* q, unsigned int newValue)
{
	int size = (sizeof(q->arr)) / (sizeof((q->arr)[0]));

	if (isFull(q))
	{
		std::cout << "There is no room in the queue\n" << std::endl;
	}
	else if (isEmpty(q))
	{
		q->arr[FRONT_CELL] = 2;
		q->arr[REAR_CELL] = 2;
	}
	else
	{
		if (q->arr[REAR_CELL] + 1 == size)
		{
			q->arr[REAR_CELL] = 2;
		}
		else
		{
			q->arr[REAR_CELL]++;
		}
	}
	q->arr[q->arr[REAR_CELL]] = newValue;
}


/*
This func will dequeue a number from the queue.
input: pointer to a queue.
output: the value that was dequeued.
*/
int dequeue(queue* q)
{
	int size = (sizeof(q->arr)) / (sizeof((q->arr)[0])), valReturn = 0;

	if (isEmpty(q))
	{
		return -1;
	}
	else if (isEmpty(q))
	{
		q->arr[FRONT_CELL] = -1;
		q->arr[REAR_CELL] = -1;
		return -1;
	}
	else
	{
		valReturn = q->arr[q->arr[FRONT_CELL]];
		if (q->arr[FRONT_CELL] + 1 == size)
		{
			q->arr[FRONT_CELL] = 2;
		}
		else
		{
			q->arr[FRONT_CELL]++;
		}
	}
	return valReturn;
}

/*
-------------------------------------------------------------------------------------------------------------------------
I didn't understand the task AT ALL! It was unclear.
I used the "front" and "rear" cells in order to avoid global vals and the prog could handle more then one queue at a time.
*/
