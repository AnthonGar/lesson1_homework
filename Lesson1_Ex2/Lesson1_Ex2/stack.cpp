#include "Stack.h"
#include <iostream>

void push(stack* s, unsigned int element);
int pop(stack* s); // Return -1 if stack is empty

void initStack(stack* s);
void cleanStack(stack* s);

void initStack(stack* s)
{
	stack* newStack = new stack;
}

void cleanStack(stack* s)
{
	stack* temp = 0;
	stack* curr = s;

	while(curr)
	{
		temp = curr;
		curr = curr->next;
		delete[] temp;
	}
}

void push(stack* s, unsigned int element)
{
	stack* temp = new stack;
	temp->number = element;
	temp->next = s;
	s = temp;
}

int pop(stack* s)
{
	int valReturn = 0;
	stack* temp = new stack;
	if (s == 0)
	{
		return -1;
	}
	else
	{
		valReturn = s->number;
		temp = s;
		s = s->next;
		delete[] temp;
	}
	return valReturn;
}