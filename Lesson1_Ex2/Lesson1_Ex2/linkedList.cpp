#include "linkedList.h"
#include <iostream>

#define NULL 0 

void addNode(Node** head, int number);
void deleteNode(Node** head);

void addNode(Node** head, int number)
{
	Node* newNode = new Node();
	newNode->number = number;
	if(*head != NULL)//If there is a head.
	{
		newNode->next = *head;
	}
	*head = newNode;
}

void deleteNode(Node** head)
{
	if (head != NULL)
	{
		Node* temp = *head;
		*head = (*head)->next;
		
		delete temp;
	}
}
