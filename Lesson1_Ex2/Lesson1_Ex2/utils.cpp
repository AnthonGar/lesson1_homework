#include <iostream>
#include "utils.h"
#include "Stack.h"
#include <conio.h>

void reverse(int* nums, unsigned int size);
int* reverse10();

void reverse(int* nums, unsigned int size)
{
	unsigned int i = 0;

	stack* reversStack = new stack;
	initStack(reversStack);

	for (i = size-1; i >= 0; i--)
	{
		push(reversStack, nums[i]);
	}

	for (i = 0; i < size; i++)
	{
		nums[i] = reversStack->number;
		reversStack = reversStack->next;
	}
}

int* reverse10()
{
	int* input = new int[10];
	int* reverse = new int[10];
	int i = 0, digit;
	
	std::cout << "Enter 10 numbers" << std::endl;

	for (i = 0; i < 10; i++)
	{
		std::cin >> digit;
		input[i] = digit;
	}

	for (i = 9; i >= 0; i--)
	{
		reverse[10 - i] = input[i];
	}
	return reverse;
}