#ifndef LINKEDLIST_H
#define LINKEDLIST_H

struct Node
{
	unsigned int number;
	Node* next ;
};

void addNode(Node** head,int number);
void deleteNode(Node** head);

#endif /* LINKEDLIST_H */